defmodule ExsubsTest do
  use ExUnit.Case

  describe "duration" do
    test "computes duration" do
      event = Exsubs.Event.new(end: ~T[00:00:05])
      assert 5 == Exsubs.Event.duration(event)
    end
  end

  describe "Event.shift" do
    test "shifts time" do
      event = Exsubs.Event.new(end: ~T[00:00:05])
      result = Exsubs.Event.shift(event, 2)

      assert result.start == ~T[00:00:02]
      assert result.end == ~T[00:00:07]
    end
  end

  describe "load" do
    test "load from file" do
      raw = File.read!("./test/test.srt")
      subs = Exsubs.load(raw)
      assert subs.format == :srt
      event = subs.events |> List.first()
      assert event.text == "Minioner."
    end
  end

  describe "Format: encode" do
    test "encode from file" do
      raw = File.read!("./test/test.srt")
      subs = Exsubs.load(raw)
      assert raw == Exsubs.Format.Subrip.encode(subs)
    end
  end

  describe "shift" do
    test "shifts time" do
      raw = File.read!("./test/test.srt")
      subs = Exsubs.load(raw)

      %{events: [event | _]} = Exsubs.shift(subs, 2)
      assert event.start == ~T[00:01:42.760]
      assert event.end == ~T[00:01:44.040]
    end
  end
end
