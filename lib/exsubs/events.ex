defmodule Exsubs.Event do
  defstruct [:start, :end, :text, type: :dialogue]

  @type t :: %__MODULE__{
    start: Time.t(),
    end: Time.t(),
    text: String.t(),
    type: atom()
  }

  def new(opts \\ []) do
    %__MODULE__{start: ~T[00:00:00], end: ~T[00:00:00]}
    |> struct(opts)
  end

  def duration(%{start: start_ms, end: end_ms} = _event, unit \\ :second) do
    Time.diff(end_ms, start_ms, unit)
  end

  def shift(event, duration, unit \\ :second) do
    event
    |> Map.update!(:start, fn start -> Time.add(start, duration, unit) end)
    |> Map.update!(:end, fn end_t -> Time.add(end_t, duration, unit) end)
  end
end
