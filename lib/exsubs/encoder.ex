defmodule Exsubs.Encoder do

  @callback encode(Exsubs.t()) :: {:ok, String.t()}
  @callback encode!(Exsubs.t()) :: String.t()
end
