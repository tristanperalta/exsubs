defmodule Exsubs.Format.Subrip do
  alias Exsubs.Utils

  def decode(raw) do
    raw
    |> Utils.split_lines()
    |> Enum.map(&parse_line/1)
    |> then(&Exsubs.new(events: &1, format: :srt))
  end

  def parse(raw), do: decode(raw)

  defp parse_line(line) do
    line
    |> String.split("\n", trim: true)
    |> build_event()
  end

  defp build_event([_, timing | text]) do
    [start_t, end_t] = String.split(timing, " --> ")
    Exsubs.Event.new(
      start: Time.from_iso8601!(start_t), 
      end: Time.from_iso8601!(end_t), 
      text: Enum.join(text, "\n")
    )
  end

  def encode(%Exsubs{events: events} = _subtitle) do
    events
    |> Enum.with_index(1)
    |> Enum.map(&build(&1))
    |> Enum.join("\n\n")
    |> Utils.add_closing_line_breaks()
  end

  def format(subtitle), do: encode(subtitle)

  defp build({%{start: start_t, end: end_t, text: text}, idx}) do
    [
      "#{idx}",
      "#{Utils.format_time(start_t, ",")} --> #{Utils.format_time(end_t, ",")}",
      text
    ] 
    |> Enum.join("\n")
  end
end
