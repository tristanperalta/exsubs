defmodule Exsubs do

  defstruct [events: [], format: :srt]

  @type t :: %__MODULE__{
    events: [Exsubs.Event.t()],
    format: String.t()
  }

  def new(opts \\ []) do
    struct!(%__MODULE__{}, opts)
  end

  def load(text) when is_binary(text) do
    Exsubs.Format.Subrip.decode(text)
  end

  def shift(subtitle, duration) do
    subtitle
    |> Map.update!(:events, fn events ->
      Enum.map(events, &Exsubs.Event.shift(&1, duration))
    end)
  end
end
